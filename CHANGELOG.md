## [6.2.1](https://gitlab.com/kilianpaquier/kilianpaquier.gitlab.io/compare/v6.2.0...v6.2.1) (2024-01-16)


### Bug Fixes

* captions order in french experiences ([c95624a](https://gitlab.com/kilianpaquier/kilianpaquier.gitlab.io/commit/c95624a252cab3c570edf851555cef9e73f77810))

## [6.2.0](https://gitlab.com/kilianpaquier/kilianpaquier.gitlab.io/compare/v6.1.6...v6.2.0) (2024-01-14)


### Features

* add copyright from hugot ([82be48c](https://gitlab.com/kilianpaquier/kilianpaquier.gitlab.io/commit/82be48c132888293ff71004c82ad9763ab94085c))


### Chores

* added latest missions and remove k3d as it was too little used ([16f0044](https://gitlab.com/kilianpaquier/kilianpaquier.gitlab.io/commit/16f0044937bafa5f606d2c27a7dbbdd6a8a8e734))
* inverted yait and cicd projects ([9e9e9a2](https://gitlab.com/kilianpaquier/kilianpaquier.gitlab.io/commit/9e9e9a253f798cb85a3fad8e9da755e1e5f7441e))

## [6.1.6](https://gitlab.com/kilianpaquier/kilianpaquier.gitlab.io/compare/v6.1.5...v6.1.6) (2023-11-21)


### Bug Fixes

* path changed for yait ([a04c824](https://gitlab.com/kilianpaquier/kilianpaquier.gitlab.io/commit/a04c8240d9ca07e0d83f59a6086ade021f7187b2))

## [6.1.5](https://gitlab.com/kilianpaquier/kilianpaquier.gitlab.io/compare/v6.1.4...v6.1.5) (2023-11-19)


### Chores

* **ci:** remove useless go include ([aee52e8](https://gitlab.com/kilianpaquier/kilianpaquier.gitlab.io/commit/aee52e8a5122a514a4a4c8cfb213ba99a7c29ff8))
* **ci:** removed publish after v4 cicd templates ([a76b779](https://gitlab.com/kilianpaquier/kilianpaquier.gitlab.io/commit/a76b779a0b55aa34ab9f5fae74a23664c1b8c5ed))
* fix cicd project name ([3dc8643](https://gitlab.com/kilianpaquier/kilianpaquier.gitlab.io/commit/3dc864314de17e9ebcf4310de04000cd6ac4d806))
* **license:** update ([335fadc](https://gitlab.com/kilianpaquier/kilianpaquier.gitlab.io/commit/335fadc2e2debb898df20dd6ae4a1e0234c1964c))
* update go dependencies ([f40cdab](https://gitlab.com/kilianpaquier/kilianpaquier.gitlab.io/commit/f40cdaba0e3dd59f9897f07ff7e4502fd42629c9))
* update profile image ([a2c32e5](https://gitlab.com/kilianpaquier/kilianpaquier.gitlab.io/commit/a2c32e5ff4519dfc385ee8abd67cc169fb6ec5df))
