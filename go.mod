module gitlab.com/kilianpaquier/kilianpaquier.gitlab.io

go 1.21

require gitlab.com/kilianpaquier/hugot v1.2.0 // indirect

// replace gitlab.com/kilianpaquier/hugot => /home/kilianpaquier/kilianpaquier/hugot
